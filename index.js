'use strict'


const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const app = require('./app');
const config = require('./config');

mongoose.connect(config.db, (error, res) => {
    if(error) throw error
    
    console.log('DB connected')

    app.listen(config.port, () => {
        console.log('Server running');
    })

})

