'use strict'

const express = require('express');
const TaskController = require('../controllers/task')
const api = express.Router()

api.get('/tasks', TaskController.getTasks );
api.get('/tasks/:id', TaskController.getTask )
api.post('/tasks/create', TaskController.saveTask)
api.put('/tasks/:id', TaskController.updateTask)
api.delete('/tasks/destroy/:id', TaskController.deleteTask )

module.exports = api;