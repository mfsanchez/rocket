'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema

const TaskSchema = Schema({
    name: String,
    dueDate: Date,
    priority: { type: Number, enum: [1,2,3,4,5]}
})

module.exports = mongoose.model('Task', TaskSchema);