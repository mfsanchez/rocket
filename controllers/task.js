'use strict'

const Task = require('./../models/task');


function getTask(req, res){
    let id = req.params.id
    
    Task.findById(id, (err, task) => {
        if(err) return res.status(500).send({message: `Error: ${err}`})
        if(!task) return res.status(404).send({message: `La tarea no existe`})
        
        res.send({task})
    })
}

function getTasks(req, res){

    Task.find({}, (err, tasks) => {
        
        if(err) return res.status(500).send({message: `Error: ${err}`})
            if(!tasks) return res.status(404).send({message: `No hay tareas`})

        res.send(200, {tasks});

    })

}

function updateTask(req, res){
    let id = req.params.id
    let updtask = req.body
    Task.findByIdAndUpdate(id, updtask, (err, task) => {
        if(err) res.status(500).send({message: `Error: ${err}`})
        res.send({task})
    })
}

function deleteTask(req, res){
    let id = req.params.id
    Task.findByIdAndRemove(id, (err) => {
        if(err) res.status(500).send({message: `Error: ${err}`})
        res.send({message: 'Tarea Eliminada'});
    })
}

function saveTask(req, res){
    let task = new Task()
    task.name = req.body.name;
    task.dueDate = req.body.dueDate;
    task.priority =  req.body.priority;

    task.save((err, taskstored) => {

        if(err) handleError(err);

        res.send({task: taskstored});
    })
}

module.exports = {
    getTask,
    getTasks,
    updateTask,
    deleteTask,
    saveTask
}